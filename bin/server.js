const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const { CommandServer, CommandServerConfig} = require('@hpierce1102/command-server');
const fs = require('fs');
const path = new require('path');
const http = new require('http').Server(app);


CommandServerConfig.createFromFile(__dirname + '/../config.json').then((config) => {
    CommandServer.init(config);
});


app.use(bodyParser.json());

app.get('/example', function(req, res){
    res.sendFile(path.resolve(__dirname + '/../web/example/index.html'));
});

app.get('/', function(req, res){
    res.sendFile(path.resolve(__dirname + '/../web/index.html'));
});

app.put('/whitelist', function(req, res){
    let body = {
        whitelist: req.body
    };

    fs.writeFileSync(fs.realpathSync(__dirname + '\\..\\config.json'), JSON.stringify(body));

    res.sendFile(path.resolve(__dirname + '/../web/index.html'));
});

app.get('*', function(req, res) {
    res.sendFile(path.resolve(__dirname + '/../web' + req.originalUrl));
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});