import Command from './Command.js';
import CommandCollection from './CommandCollection.js';
import CommandCollectionDOMModifier from "./CommandCollectionDOMModifier.js";

const collection = new CommandCollection();
const systemDom = new CommandCollectionDOMModifier();
const socket = io('http://localhost:3001');

document.querySelector('#new-ping').addEventListener('click', function(){
    let command = 'ping'; //document.querySelector('input[name="command"]').value;
    let args = document.querySelector('input[name="new-ping-site"]').value;
    args = args.split(' ');

    socket.emit('system.attach', {
        command : command,
        arguments : args
    });
});

socket.on('system.attached', function(msg) {
    let command = new Command(msg.token, socket);
    collection.addCommand(command);
});

socket.on('system.detached', function(msg) {
    let command = collection.getCommand(msg.token);
    collection.removeCommand(command);
});

socket.on('system.error', function(msg){
    systemDom.writeSystemMessage('system.error - ' + msg.message);
});

socket.on('command.output', function(msg) {
    let command = collection.getCommand(msg.token);
    command.writeOutput('command.output - ' + msg.output);
});

socket.on('command.error', function(msg) {
    let command = collection.getCommand(msg.token);
    command.writeOutput('command.error - ' + msg.output);
});

socket.on('command.terminated', function(msg) {
    let command = collection.getCommand(msg.token);
    command.writeOutput('command.terminated');
});

socket.on('command.init', function(msg) {
    let command = collection.getCommand(msg.token);
    command.writeOutput('command.init');
});