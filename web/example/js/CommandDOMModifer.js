export default class CommandDOMModifer {
    constructor (command) {
        this.command = command;
    }

    attachKillListeners() {
        document.querySelector('.command-runner-item[data-token="' + this.command.token + '"] .kill-command')
            .addEventListener('click', systemDetachFactory(this.command));
    }

    writeOutput(message) {
        let terminal = document.querySelector('.command-runner-item[data-token="' + this.command.token + '"] .terminal');
        let lineTemplate = document.querySelector('#terminal-line-item-template');
        let lineItem = document.importNode(lineTemplate.content, true);
        let lineItemText = lineItem.querySelector('.line-item-text');
        lineItemText.innerHTML = message;

        terminal.appendChild(lineItem);
    }
}

function systemDetachFactory(command) {
   return function() {
        command.kill();
    };
}


