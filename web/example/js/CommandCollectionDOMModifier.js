export default class CommandCollectionDOMModifier {
    construct() {

    }

    writeSystemMessage(msg) {
        let terminal = document.querySelector('#system-log-container .terminal');
        let lineTemplate = document.querySelector('#terminal-line-item-template');
        let lineItem = document.importNode(lineTemplate.content, true);
        let lineItemText = lineItem.querySelector('.line-item-text');
        lineItemText.innerHTML = msg;

        terminal.appendChild(lineItem);
    }

    addCommandRunner(token) {
        let list = document.querySelector('#command-runner-list');
        let runnerTemplate = document.getElementById('command-runner-item-template');
        let runnerItem = document.importNode(runnerTemplate.content, true);

        runnerItem.querySelector('.command-runner-item')
            .setAttribute('data-token', token);

        list.appendChild(runnerItem);
    }

    removeCommandRunner(token) {
        document.querySelector('.command-runner-item[data-token="' + token + '"]').remove();
    }

}