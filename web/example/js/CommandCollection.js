import CommandCollectionDOMModifer from './CommandCollectionDOMModifier.js';

const dom = new CommandCollectionDOMModifer();

export default class CommandCollection {
    constructor() {
        this.commands = [];
    }

    addCommand(command) {
        if(this.hasCommand(command)) {
            throw 'cannot add command ' + command.token + ' - command already exists.';
        }

        this.commands.push(command);

        dom.addCommandRunner(command.token);
        dom.writeSystemMessage('system.attached');

        //TODO: switch to raising an event?
        command.addKillListeners();
    }

    hasCommand(command) {
        try {
            this.getCommand(command.token);
            return true;
        } catch (e){
            return false;
        }
    }

    getCommand(token) {
        let commands = this.commands.filter((command) => {
            return token === command.token;
        });

        if(commands.length === 1) {
            return commands[0];
        } else {
            throw 'cannot get command - command does not exist.';
        }
    }

    removeCommand(command) {
        if(!this.hasCommand(command)) {
            throw 'cannot add remove ' + command.token + ' - command does not exist.';
        }

        this.commands = this.commands.filter((commandInCollection) => {
            return command.token !== commandInCollection.token;
        });

        dom.removeCommandRunner(command.token);
        dom.writeSystemMessage('system.detached');
    }
}