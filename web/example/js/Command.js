import CommandDOMModifer from './CommandDOMModifer.js';

export default class Command {
    constructor(token, socket) {
        this.token = token;
        this.socket = socket;

        this.dom = new CommandDOMModifer(this);
    }

    addKillListeners() {
        this.dom.attachKillListeners();
    }

    kill() {
        this.socket.emit('system.detach', {
            token : this.token,
        });
    }

    writeOutput(msg) {
        this.dom.writeOutput(msg);
    }
}